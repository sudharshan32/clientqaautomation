package VideoPlayerTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;
public class VideoPlayer {
	WebDriver driver;
	
	//Opening browser
	@Test(priority =0)
	public void openBrowser() {
		
		String chromeDriverPath = System.getProperty("user.dir") + "/libs/chromedriver";
		System.setProperty("webdriver.chrome.driver",chromeDriverPath);
		driver = new ChromeDriver();
		driver.get("https://vimeo.com");
	}
	
	//Navigating to player
	@Test(priority =1)
	public void navigateToPlayerAndClick() {
		WebElement inspirationTab = driver.findElement(By.xpath("//span[contains(text(),'Inspiration')]"));
		Assert.assertTrue(inspirationTab.isDisplayed()) ;
		inspirationTab.click();
		
		WebElement exploreLink = driver.findElement(By.cssSelector("a[href*='/watch']"));
		exploreLink.click();
		
		WebElement watchNow = driver.findElement(By.xpath("//*[contains(text(),'Watch now')]"));
		watchNow.click();
		
		 
	}
	
	//Verifying player controls
	@Test(priority =2)
	public void verifyPlayerControls()throws InterruptedException{
		try {
			//Verifying progress bar
			WebElement progressBar = driver.findElement(By.xpath("//div[@class='vp-progress']"));
			Actions actions = new Actions(driver);
			actions.moveToElement(progressBar);
			Assert.assertTrue(progressBar.isDisplayed()) ;
			
			//Verifying pause button and tapping on it
			WebElement pause = driver.findElement(By.xpath("//*[@class='pause-icon']"));
			actions.moveToElement(pause).perform();
			Assert.assertTrue(pause.isDisplayed()) ;
			pause.click();
			Thread.sleep(500);
			
			//Resume playing
			WebElement play = driver.findElement(By.xpath("//div[@class='play-icon']"));
			actions.moveToElement(play).perform();
			Assert.assertTrue(play.isDisplayed()) ;
			play.click();
			Thread.sleep(1000);
			
			//Changing volume
			WebElement sliderVolume = driver.findElement(By.xpath("//div[@class='volume']"));
			actions.moveToElement(sliderVolume).perform();
			Assert.assertTrue(sliderVolume.isDisplayed()) ;
			sliderVolume.click();
			
			//Tapping settings, verifying quality and selecting quality
			WebElement settings = driver.findElement(By.xpath("//button[@class='vp-prefs js-prefs']"));
			actions.moveToElement(settings).perform();
			Assert.assertTrue(settings.isDisplayed()) ;
			settings.click();
			
			WebElement verifyingQualityOptions = driver.findElement(By.xpath("//ul[@class='vp-panel-items js-panelItems']"));
			Assert.assertTrue(verifyingQualityOptions.isDisplayed()) ;
			Thread.sleep(500);
			WebElement qualityChange = driver.findElement(By.xpath("//li[contains(text(),'1080p')]"));
			actions.moveToElement(qualityChange).perform();
			qualityChange.click();
			Thread.sleep(1000);
			
			//Verifying full screen mode
			WebElement fullScreen = driver.findElement(By.xpath("//button[@class='fullscreen']"));
			actions.moveToElement(fullScreen);
			actions.perform();
			Assert.assertTrue(fullScreen.isDisplayed()) ;
			fullScreen.click();
			Thread.sleep(100);
			
			//Exiting full screen mode
			WebElement exitFullScreen = driver.findElement(By.xpath("//div[@class='unfullscreen-icon']"));
			actions.moveToElement(exitFullScreen);
			actions.perform();
			Assert.assertTrue(exitFullScreen.isDisplayed()) ;
			exitFullScreen.click();
			Thread.sleep(1000);
			
			
		} catch (Error e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//Close browser
	@Test(priority =3)
	public void closeBrowser()throws InterruptedException{
		Thread.sleep(1000);
		driver.quit();
	}
}